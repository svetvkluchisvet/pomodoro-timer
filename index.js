window.onload = function () {

    let state = document.getElementById('state');

    let clockDisplay = document.getElementById('clockDisplay');
    let timerDisplay = document.getElementById('timerDisplay');
    let breakDisplay = document.getElementById('breakDisplay');
    let btnUp = document.getElementsByClassName('btnUp');
    let btnDown = document.getElementsByClassName('btnDown');
    let btnRun = document.getElementById('run');
    let sessionCountText = document.getElementById('sessionsCount');
    let breakCountText = document.getElementById('breaksCount');

    clockDisplay.innerHTML = '25:00';

    let running = false;
    let sessionCount = 0;
    let breakCount = 0;
    let timer = false;
    let timerTime = '25';
    let breakTime = '5';

    let timeInterval;
    /*Предупреждение!
    * Каждый раз когда срабатывает этот звук, у меня случается микроинфаркт.
    * Но он прикольный, поэтому я его выбрала
    * */
    let audio = new Audio('./public/sounds/smeh-minonov-prikolnyy-rington-sms.mp3');
    let audio2 = new Audio('./public/sounds/data-readout_mj7jf_e_.mp3');

    for (let x = 0; x < btnUp.length; x++) {
        btnUp[x].addEventListener("click", function (e) {
            if (!running) {
                values('add', this.value);
            }

        });
    }

    for (let x = 0; x < btnDown.length; x++) {
        btnDown[x].addEventListener("click", function (e) {
            if (!running) {
                values('substract', this.value);
            }
        });
    }

    btnRun.addEventListener("click", function (e) {

        timer = !timer;

        running = !running;

        if (running) {
            btnRun.innerHTML = 'СТОП';
            startSession();
        } else {
            btnRun.innerHTML = 'СТАРТ';
            stop();
        }

    });

    let startSession = () => {

        running = true;
        breakCountText.innerHTML = breakCount;
        sessionCountText.innerHTML = sessionCount;
        state.innerHTML = "Работаем!";

        if (timer) {
            pomodoro(timerTime);
        }
    }
    let breakSession = () => {

        sessionCountText.innerHTML = sessionCount;
        state.innerHTML = "Отдыхаем!";

        if (!timer) {
            pomodoro(breakTime);
        }
    }
    let stop = () => {
        clearInterval(timeInterval);
        sessionCount = 0;
        breakCount = 0;
        running = false;
        timer = false;
    }
    let pomodoro = (time) => {

        let clock = time - 1;
        let seconds = 60;
        let forProgress = time * 60;
        let progressCounter = time * 60;
        let progressPercentage;

        timeInterval = setInterval(function () {

            seconds--;
            progressCounter--;

            if (seconds === 0) {

                clockDisplay.innerHTML = clock + ':00';
                clock--;

                if (clock === -1) {
                    clearInterval(timeInterval);
                    timer = !timer;
                    if (timer) {
                        breakCount++;
                        audio2.play();
                        startSession();
                    } else {
                        sessionCount++;
                        audio.play();
                        breakSession();
                    }
                }

                seconds = 59;

            } else {

                if (seconds < 10) {
                    clockDisplay.innerHTML = clock + ':0' + seconds;
                } else {
                    clockDisplay.innerHTML = clock + ':' + seconds;
                }

            }

            progressPercentage = Math.round((progressCounter * 100) / forProgress);

        }, 1000);

    }

    let values = (input, parameter) => {

        let value;
        if (parameter === 'timer') {
            value = timerDisplay.innerHTML;
        } else if (parameter === 'breakD') {
            value = breakDisplay.innerHTML;
        }

        if (input === 'add') {
            value++;
        } else if (input === 'substract') {
            if (value > 1) {
                value--;
            } else {
                value = 1;
            }

        }

        if (parameter === 'timer') {

            timerDisplay.innerHTML = value;

            if (value < 10) {
                clockDisplay.innerHTML = '0' + value + ':00';
            } else {
                clockDisplay.innerHTML = value + ':00';
            }

            timerTime = value;

        } else if (parameter === 'breakD') {

            breakDisplay.innerHTML = value;

            breakTime = value;
        }

    }

}